﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
    public static class Level
    {
        public static HUD HUDObjects, HUDScore, HUDPause;
        public static Player player;
        public static Background background;
        public static int score;

        public static void Create() {
            EnemySpawner.AllowSpawn = true;
            Core.window.KeyPressed += new EventHandler<KeyEventArgs>(OnKeyPressed);
            Core.window.SetKeyRepeatEnabled(false);
            background = new Background();
            player = new Player();
            Core.Tick += new Core.VoidDelegate(EnemySpawner.TickHandler);
            Core.Tick += new Core.VoidDelegate(TickHandler);
            Core.Tick += new Core.VoidDelegate(CollisionDetector.TickHandler);
            HUDObjects = new HUD(new Vector2f(0, 15), "Objects=", true);
            HUDScore = new HUD(new Vector2f(0, 105), "Score=", true);
            HUDPause = new HUD(new Vector2f(Core.windowWidth / 2 - 35, Core.windowHeight / 2), "Пауза", false);
            HUDPause.Font = Resources.menuFont;
            HUDPause.Size = 20;
            HUDPause.Hide();
            score = 0;
        }

        public static void Destroy() {
            EnemySpawner.AllowSpawn = false;
            background.Dispose();
            foreach (Vessel vessel in Core.alliesList) vessel.Hit(9999);
            Core.alliesList.Clear();
            foreach (Vessel vessel in Core.enemiesList) vessel.Hit(9999);
            Core.enemiesList.Clear();
            foreach (Bullet bullet in Core.allyBulletsList) bullet.Destroy();
            Core.allyBulletsList.Clear();
            foreach (Bullet bullet in Core.enemyBulletsList) bullet.Destroy();
            Core.enemyBulletsList.Clear();
            foreach (Item item in Core.itemsList) item.Destroy();
            Core.itemsList.Clear();
            Core.Tick -= new Core.VoidDelegate(EnemySpawner.TickHandler);
            Core.Tick -= new Core.VoidDelegate(TickHandler);
            Core.Tick -= new Core.VoidDelegate(CollisionDetector.TickHandler);
            Core.window.KeyPressed -= new EventHandler<KeyEventArgs>(OnKeyPressed);
            HUDObjects.Destroy();
            HUDPause.Destroy();
            HUDScore.Destroy();
        }

        static void OnKeyPressed(object sender, KeyEventArgs e) {
            RenderWindow sWindow = (RenderWindow)sender;
            switch (e.Code) {
                case (Keyboard.Key.Escape): sWindow.Close(); break;
                case (Keyboard.Key.I): if (Core.isDebugInfo) new Ammo(Core.camera.Center); break;
                case (Keyboard.Key.K): if (Core.isDebugInfo) new Health(Core.camera.Center); break;
                case (Keyboard.Key.M): if (Core.isDebugInfo) new Shield(Core.camera.Center); break;
                case (Keyboard.Key.P): Pause(); break;
            }
        }

        static void TickHandler() {
            HUDObjects.valueP = Core.enemiesList.Count + Core.alliesList.Count + Core.allyBulletsList.Count + Core.enemyBulletsList.Count + Core.itemsList.Count;
            HUDScore.valueP = score;
        }

        static void Pause() {
            if (Core.Pause) {
                Core.Pause = false;
                HUDPause.Hide();
            }
            else {
                Core.Pause = true;
                HUDPause.Show();
            }
        }
    }

}
