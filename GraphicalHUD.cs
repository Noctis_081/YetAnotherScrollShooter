﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
    public class GraphicalHUD
    {
        Sprite extSprite, intSprite;
        Vector2f extPosition, intPosition;
        const int borderOffset = 4;
        int intWidth=0, intHeight=24, redPoint=28;
        bool isActive = true;

        public GraphicalHUD(Vector2f position, Color color) {
            extSprite = new Sprite(Resources.GHUDExternal);
            intSprite = new Sprite(Resources.GHUDInternal);
            extSprite.Color = color;
            Core.Draw += new Core.ByteDelegate(this.Draw);
            Core.Tick += new Core.VoidDelegate(this.TickHandler);
            this.extPosition = position;
            position.Y += borderOffset;
            position.X += borderOffset-1;
            this.intPosition = position;
        }

        void TickHandler() {
            intSprite.TextureRect = new IntRect(0, 0, intWidth, intHeight);
            intPosition.Y += Core.cameraSpeed;
            extPosition.Y += Core.cameraSpeed;
            extSprite.Position = extPosition;
            intSprite.Position = intPosition;
            if (intWidth < redPoint) intSprite.Color = new Color(255, 0, 0);
            else intSprite.Color = new Color(255, 255, 255);
            if (!isActive) Dispose();
        }

        void Draw(byte layer) {
            if (layer == 4) {
                Core.window.Draw(intSprite);
                Core.window.Draw(extSprite);
            }
        }

        void Dispose() {
            Core.Draw -= new Core.ByteDelegate(this.Draw);
            Core.Tick -= new Core.VoidDelegate(this.TickHandler);
            extSprite.Dispose();
            intSprite.Dispose();
        }

        public void Destroy() { isActive = false; }

        public int Percentage { set { intWidth = value * 7 / 5; } }
    }
}
