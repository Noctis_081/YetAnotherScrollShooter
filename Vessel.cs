using System;
using SFML.Graphics;
using SFML.Window;
using SFML.Audio;
using System.Diagnostics;

namespace YetAnotherScrollShooter
{
	public abstract class Vessel:Entity
	{
		protected int hp, speed, weaponDelay, weaponDamage, sp, maxHP, maxSP, missilesAmount, shieldsDelay, maxMissiles, weaponCoolDown, scorePoints=0;
		protected int shootDelay, missileDelay;
        protected bool isAlly=false, isGodMode=false;

		public Vessel () {
			shieldsDelay = 60;
            weaponCoolDown = 0;
			maxAnimationFrames = 3;
            missilesAmount = 0;
			maxMissiles = 30;
			maxHP = 100;
			hp = maxHP;
			speed = 3;
			weaponDelay = 10;
			maxSP = 0;
			sp = maxSP;
			keepPosition = true;
		}

        public void AddMissile(int amount) {
            missilesAmount += amount;
        }

		public void Heal(int amount) {
			if (hp + amount < maxHP) hp += amount;
			else hp = maxHP;
		}

		public void RestoreShields(int amount) {
			if (sp + amount < maxSP) sp += amount;
			else sp = maxSP;
		}

		public void Hit(int amount) {
			if (isGodMode) { 
				Paint (15, 255, 255, 255, 155);
			}
			else {
				if (sp >= amount) Paint (15, 100, 100, 255, 255);
				else Paint (15, 255, 100, 100, 255);
				if (sp < amount) {
					amount -= sp;
					sp = 0;
				} 
				else {
					sp -= amount;
					amount = 0;
				}
				if (amount < hp) hp -= amount;
				else hp = 0;
			}
		}

        public void shootMissile() {
            float tempPosX = 0, tempPosY = 0;
            if (isAlly) { tempPosX = position.X + width / 2; tempPosY = position.Y + height; }
            else { tempPosX = position.X - width / 2; tempPosY = position.Y - height / 2; }
            if (missilesAmount > 0||isGodMode) {
                missilesAmount--;
                Vector2f missilePosition = new Vector2f(tempPosX, tempPosY);
                new Missile(missilePosition, isAlly);
            }
        }

		protected void Shoot() {
            float tempPosX=0, tempPosY=0;
            if (isAlly) { tempPosX = position.X + width / 2; tempPosY = position.Y - height/2 + 10; }
            else { tempPosX = position.X - width / 2; tempPosY = position.Y + height/2; }
            if (weaponCoolDown>=weaponDelay) {
                weaponCoolDown = 0;
                Vector2f bulletPosition = new Vector2f(tempPosX,tempPosY);
                new Plasmoid(bulletPosition, isAlly);
            }
		}

		public virtual void Destroy() {
			Vector2f tempPosition = position;
            if (!isAlly) {
			tempPosition.Y -= height / 2;
			tempPosition.X -= width / 2;
			new Explosion (tempPosition);
                int temp = Core.random.Next(10);
                switch (temp) {
                    case 0: new Ammo(tempPosition); break;
                    case 1: new Health(tempPosition); break;
                    case 2: new Shield(tempPosition); break;
                }
                Level.score += scorePoints;
            }
            else {
                tempPosition.Y += height / 2;
                tempPosition.X += width / 2;
                new Explosion(tempPosition);
            }
            Dispose();
		}

		public void Move(char direction) {
			switch (direction) {
				case ('u'): position.Y -= speed; break; 
				case ('d'): position.Y += speed; break;
				case ('r'): position.X += speed; break;
				case ('l'): position.X -= speed; break;
			}
		}

		protected override void TickHandler () {
            weaponCoolDown++;
			if (Core.tickCounter % shieldsDelay == 0) RestoreShields (5);
			base.TickHandler ();
            if (hp == 0) Destroy();
		}
	}




	

	public class Striker:Vessel 
    {

		public Striker(Vector2f position) {
			texture = Resources.strikerTexture;
			maxHP = 100;
			hp = maxHP;
			width = 35;
			height = 47;
			MakeSprite ();
			sprite.Rotation=180;
			shootDelay = 15;
			missileDelay = 30;
			missilesAmount = 2;
			scorePoints = 20;
			speed = 5;
			Core.enemiesList.Add(this);
			this.position = position;
		}

		override protected void TickHandler() {
            if (Level.player.Position.X + Level.player.Width > position.X) position.X += speed;
            if (Level.player.Position.X + Level.player.Width < position.X) position.X -= speed;
            if (position.Y < Level.player.Position.Y) position.Y += speed / 2;
			else position.Y += speed;
            if (Math.Abs(position.X - Level.player.Position.X - Level.player.Width) <= 15 && Core.tickCounter % shootDelay == 0) Shoot();
            if (Math.Abs(position.X - Level.player.Position.X - Level.player.Width) <= 7 && Core.tickCounter % missileDelay == 0) shootMissile();
			base.TickHandler ();
		}

		protected override void Dispose () {
			base.Dispose ();
			Core.enemiesList.Remove (this);
		}
	}

	public class Bomber:Vessel
	{

		public Bomber(Vector2f position) {
			texture = Resources.bomberTexture;
			maxHP = 300;
			hp = maxHP;
			maxSP = 700;
			sp = maxSP;
			width = 59;
			height = 59;
			MakeSprite ();
			sprite.Rotation=180;
			shootDelay = 20;
			missileDelay = 30;
			missilesAmount = 10;
			scorePoints = 50;
			speed = 3;
			Core.enemiesList.Add(this);
			this.position = position;
		}

		override protected void TickHandler() {
            if (Level.player.Position.X + Level.player.Width > position.X) position.X += speed;
            if (Level.player.Position.X + Level.player.Width < position.X) position.X -= speed;
            if (position.Y < Level.player.Position.Y) position.Y += 0.3f;
            else position.Y += speed;
            if (Math.Abs(position.X - Level.player.Position.X - Level.player.Width) <= 30 && Core.tickCounter % shootDelay == 0) Shoot();
            if (Math.Abs(position.X - Level.player.Position.X - Level.player.Width) <= 20 && Core.tickCounter % missileDelay == 0) shootMissile();
			base.TickHandler ();
		}

		protected override void Dispose () {
			base.Dispose ();
			Core.enemiesList.Remove (this);
		}
	}

	public class Scout:Vessel
	{

		public Scout(Vector2f position) {
			texture = Resources.scoutTexture;
			maxHP = 50;
			hp = maxHP;
			width = 33;
			height = 39;
			MakeSprite ();
			sprite.Rotation=180;
			shootDelay = 15;
			missileDelay = 120;
			missilesAmount = 0;
			scorePoints = 10;
			speed = 7;
			Core.enemiesList.Add(this);
			this.position = position;
		}

		override protected void TickHandler() {
			position.Y += speed;
			if (Core.tickCounter%weaponDelay==0) Shoot ();
			base.TickHandler ();
		}

		protected override void Dispose () {
			base.Dispose ();
			Core.enemiesList.Remove (this);
		}
	}

	public class Fighter:Vessel
	{
		bool isInDanger=false, isGoingBack=false;
		int dangerDelay,goingBackDelay;
		Bullet danger;
		Vector2f absPosition;

		public Fighter(Vector2f position) {
			texture = Resources.fighterTexture;
			maxHP = 50;
			hp = maxHP;
			width = 41;
			height = 47;
			MakeSprite ();
			sprite.Rotation=180;
			shootDelay = 5;
			missileDelay = 120;
			missilesAmount = 0;
			scorePoints = 100;
			speed = 6;
			Core.enemiesList.Add(this);
			this.position = position;
		}

		override protected void TickHandler() {
			absPosition = position;
			absPosition.X -= width / 2;
			absPosition.Y -= height / 2;
			if (dangerDelay==0) isInDanger = false;
			if (goingBackDelay==0) isGoingBack = false;
			foreach (Bullet bullet in Core.allyBulletsList) {
				if (Core.Distance (absPosition, bullet.Position) < 80) {
					danger = bullet;
					isInDanger = true;
					dangerDelay = 10;
				}
			}
			if (isInDanger) {
				if (danger.Position.X>=position.X) position.X-=speed;
				else position.X+=speed;
			}
			else {
                if (Level.player.Position.X + Level.player.Width > position.X) position.X += speed;
                if (Level.player.Position.X + Level.player.Width < position.X) position.X -= speed;
                if (position.Y < Level.player.Position.Y && Core.Distance(position, Level.player.Position) > 70) position.Y += speed;
				else {isGoingBack = true; goingBackDelay = 30;}
                if (Math.Abs(position.X - Level.player.Position.X - Level.player.Width) <= 15 && Core.tickCounter % shootDelay == 0) Shoot();
			}
			if (isGoingBack) position.Y -= speed;
			goingBackDelay--;
			dangerDelay--;
			base.TickHandler ();
		}

		protected override void Dispose () {
			base.Dispose ();
			Core.enemiesList.Remove (this);

		}
	}

	public class Interceptor:Vessel
	{
		bool isInDanger=false;
		int dangerDelay;
		Bullet danger;
		Vector2f absPosition;

		public Interceptor(Vector2f position) {
			texture = Resources.interceptorTexture;
			maxHP = 100;
			hp = maxHP;
			width = 43;
			height = 43;
			MakeSprite ();
			sprite.Rotation=180;
			shootDelay = 5;
			missileDelay = 20;
			missilesAmount = 4;
			scorePoints = 50;
			speed = 5;
			Core.enemiesList.Add(this);
			this.position = position;
		}

		override protected void TickHandler() {
			absPosition = position;
			absPosition.X -= width / 2;
			absPosition.Y -= height / 2;
			if (dangerDelay==0) isInDanger = false;
			foreach (Bullet bullet in Core.allyBulletsList) {
				if (Core.Distance (absPosition, bullet.Position) < 80) {
					danger = bullet;
					isInDanger = true;
					dangerDelay = 10;

				}
			}
			if (isInDanger) {
				if (danger.Position.X>=position.X) position.X-=speed;
				else position.X+=speed;
			}

			else {
                if (Level.player.Position.X + Level.player.Width > position.X) position.X += speed;
                if (Level.player.Position.X + Level.player.Width < position.X) position.X -= speed;
                if (position.Y < Level.player.Position.Y && Core.Distance(position, Level.player.Position) > 80) position.Y += speed;
                if (Math.Abs(position.X - Level.player.Position.X - Level.player.Width) <= 15 && Core.tickCounter % shootDelay == 0) Shoot();
			}
			dangerDelay--;
			position.Y += speed;
			base.TickHandler ();
		}

		protected override void Dispose () {
			base.Dispose ();
			Core.enemiesList.Remove (this);
		}
	}
}
