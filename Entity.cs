using System;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
	public abstract class Entity
	{
		protected Vector2f position;
		protected Texture texture;
		protected IntRect textureWindow;
		public Sprite sprite;
		protected byte maxAnimationFrames;
		protected byte animationFrame;
		protected byte animationDelay;
		protected byte targetLayer;
        protected bool keepPosition;
		protected int width,height,paintTime;

		protected Entity() {
			targetLayer = 2;
			maxAnimationFrames = 1;
			animationFrame = 1;
			animationDelay = 10;
			width = 128; 
			height = 128;
			texture = Resources.placeholderTexture;
			position = Core.camera.Center;
			position.X -= Core.camera.Size.X/2-100;
			position.Y -= Core.camera.Size.Y/2-100;
			position.X -= width / 2;
			Core.Draw += new Core.ByteDelegate (this.Draw);
			Core.Tick += new Core.VoidDelegate (this.TickHandler);
		}

		protected Entity(Texture texture, int heightOfEntity, int widthOfEntity) {
			position = Core.camera.Center;
			position.X -= width / 2;
			width = widthOfEntity;
			height = heightOfEntity;
			textureWindow = new IntRect (0,0,width, height);
			this.texture = texture;
			sprite = new Sprite (texture, textureWindow);
			animationFrame = 1;
			animationDelay = 10;
			maxAnimationFrames = 1;
			Core.Draw += new Core.ByteDelegate (this.Draw);
			Core.Tick += new Core.VoidDelegate (this.TickHandler);
		}

		virtual protected void TickHandler() {
            if (paintTime >= 0) paintTime--;
            if (paintTime == 0) Paint(-1, 255, 255, 255, 255);
			if (keepPosition) position.Y += Core.cameraSpeed;
			if (Core.tickCounter % animationDelay == 0) NextAnimationFrame();
			if ((Core.Distance (Core.camera.Center, this.position) > 1000)) this.Dispose ();
		}

		virtual protected void Draw(byte layer) {
			if (layer == targetLayer&&EnemySpawner.AllowSpawn) {
				sprite.Position = position;
				Core.window.Draw (sprite);
			}
		}

		virtual protected void NextAnimationFrame() {
			if (animationFrame == maxAnimationFrames) {
				animationFrame = 1;
				textureWindow.Left = 0;
				sprite.TextureRect= textureWindow;
			} 
			else {
				animationFrame++;
				textureWindow.Left += width;
				sprite.TextureRect = textureWindow;
			}
		}

		virtual protected void Dispose() {
			Core.Tick -= new Core.VoidDelegate (this.TickHandler);
			Core.Draw -= new Core.ByteDelegate (this.Draw);
			sprite.Dispose ();
		}

        protected void Paint(int time, byte r, byte g, byte b, byte a) {
            paintTime = time;
            sprite.Color = new Color(r, g, b, a);
        }

        protected void MakeSprite() {
            textureWindow = new IntRect(0, 0, width, height);
            sprite = new Sprite(texture, textureWindow);
        }
	}
}