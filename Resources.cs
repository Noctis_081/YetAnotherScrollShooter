using System;
using System.IO;
using SFML.Graphics;
using SFML.Window;
using SFML.Audio;

namespace YetAnotherScrollShooter
{
	public static class Resources
	{
		public static Texture bgTexture;
		public static Texture playerTexture;
		public static Texture strikerTexture;
		public static Texture bomberTexture;
		public static Texture scoutTexture;
		public static Texture fighterTexture;
		public static Texture interceptorTexture;
		public static Texture bPlasmoidTexture;
		public static Texture gPlasmoidTexture;
		public static Texture missileTexture;
		public static Texture ammoItemTexture;
		public static Texture shieldItemTexture;
		public static Texture healthItemTexture;
		public static Texture ammoEffectTexture;
		public static Texture shieldEffectTexture;
		public static Texture healthEffectTexture;
		public static Texture smallExplosionTexture;
		public static Texture bigExplosionTexture;
		public static Texture placeholderTexture;
		public static Font hudFont;
        public static Font menuFont;
        public static SoundBuffer missileSound;
        public static SoundBuffer plasmoidSound;
        public static SoundBuffer smallExplosionSound;
        public static SoundBuffer bigExplosionSound;
        public static Texture mainMenuSplash;
        public static Texture gameOverSplash;
        public static Texture GHUDInternal;
        public static Texture GHUDExternal;
        public static Texture controls;
        public static StreamReader enemySpawnPattern;

        public static void Load() {
            bgTexture = new Texture("sprites/background2_bright.png");
            playerTexture = new Texture("sprites/player.png");
            strikerTexture = new Texture("sprites/striker.png");
            bomberTexture = new Texture("sprites/bomber.png");
            scoutTexture = new Texture("sprites/scout.png");
            fighterTexture = new Texture("sprites/fighter.png");
            interceptorTexture = new Texture("sprites/interceptor.png");
            bPlasmoidTexture = new Texture("sprites/bullet_2.png");
            gPlasmoidTexture = new Texture("sprites/bullet_1.png");
            missileTexture = new Texture("sprites/missile.png");
            ammoItemTexture = new Texture("sprites/item_ammo.png");
            shieldItemTexture = new Texture("sprites/item_shield.png");
            healthItemTexture = new Texture("sprites/item_health.png");
            ammoEffectTexture = new Texture("sprites/ammo_effect.png");
            shieldEffectTexture = new Texture("sprites/shield_effect.png");
            healthEffectTexture = new Texture("sprites/heal_effect.png");
            smallExplosionTexture = new Texture("sprites/explosion_small.png");
            bigExplosionTexture = new Texture("sprites/explosion_big.png");
            placeholderTexture = new Texture("sprites/placeholder.png");
            hudFont = new Font("fonts/font.ttf");
            menuFont = new Font("fonts/Casper_B.ttf");
            missileSound = new SoundBuffer("sounds/missile.wav");
            plasmoidSound = new SoundBuffer("sounds/plasmoid.wav");
            smallExplosionSound = new SoundBuffer("sounds/explosion_small.wav");
            bigExplosionSound = new SoundBuffer("sounds/explosion_big.wav");
            mainMenuSplash = new Texture("sprites/mainmenusplash.png");
            gameOverSplash = new Texture("sprites/gameoversplash.png");
            GHUDInternal = new Texture("sprites/HUD_bar_internal.png");
            GHUDExternal = new Texture("sprites/HUD_bar_external.png");
            controls = new Texture("sprites/controls2.png");
            try { enemySpawnPattern = new StreamReader("patterns/patterns.ptn"); }
            catch (FileNotFoundException) { throw new SFML.LoadingFailedException(); }
        }
	}
}

