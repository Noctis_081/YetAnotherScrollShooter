using System;
using SFML.Window;
using SFML.Graphics;

namespace YetAnotherScrollShooter
{
	public class Background {
		int width, height;
		Vector2f position;
		IntRect textureWindow;
		Texture texture;
		Sprite sprite;

		public Background () {
			width = 800;
			height = 5120;
			position = Core.camera.Center;
			position.X -= width / 2;
			position.Y -= 3*height / 4;
			textureWindow = new IntRect (0,0,width,height);
			texture = Resources.bgTexture;
			sprite = new Sprite (texture, textureWindow);
			Core.Draw += new Core.ByteDelegate (this.Draw);
			Core.Tick += new Core.VoidDelegate (this.TickHandler);
		}

		void TickHandler() {
			if (Math.Abs(position.Y-(int)Core.camera.Center.Y)==height/4) {
				position.Y -= height/2;
			}
		}

		void Draw(byte layer) {
			if (layer == 0) {
				sprite.Position = position;
				Core.window.Draw (sprite);
			}
		}

		public void Dispose() {
			Core.Draw -= new Core.ByteDelegate (this.Draw);
			Core.Tick -= new Core.VoidDelegate (this.TickHandler);
			sprite.Dispose ();
		}
	}
}

