using System;
using SFML.Graphics;
using SFML.Window;
using System.Diagnostics;
using System.Collections.Generic;
using System.Windows.Forms;


namespace YetAnotherScrollShooter
{
	public class Core
	{
		public static List<object> enemiesList = new List<object>(), alliesList = new List<object>();
        public static List<object> enemyBulletsList = new List<object>(), allyBulletsList = new List<object>();
		public static List<object> itemsList = new List<object>();
		public const int windowWidth=600, windowHeight=800, tickCounterLimit=60*1000, defaultCameraSpeed = -1;
        static int tickDelay = 16;
		public static SFML.Graphics.View camera = new SFML.Graphics.View (new FloatRect (0, 0, windowWidth, windowHeight));
		public static RenderWindow window = new RenderWindow (new VideoMode (windowWidth, windowHeight), "Yet Another Scroll-Shooter Game",Styles.Close);
		public static int tickCounter=0, cameraSpeed = 0;
		public static Stopwatch clock = new Stopwatch();
		public delegate void VoidDelegate ();
		public delegate void ByteDelegate (byte layer);
		public static event VoidDelegate Tick;
		public static event ByteDelegate Draw;
		public static bool isPause = false, isDebugInfo=false, isVsync=false;
		public static Random random = new Random ();
        public static HUD HUDFPS, HUDVsync;
        public static Color windowColor;

		public static void Main (string[] args) {
            Initialize();
            Menu();
			while (window.IsOpen()) {
				if (clock.ElapsedMilliseconds >= tickDelay) {
					window.DispatchEvents ();
					if (!isPause) Tick ();
                    window.Clear(windowColor);
				    Draw (0);
					Draw (1);
					Draw (2);
                    Draw (3);
                    Draw (4);
					window.Display ();
					FPS ();
					clock.Restart ();
				}
			}
		}

        static void OnKeyPressed(object sender, SFML.Window.KeyEventArgs e) {
            RenderWindow sWindow = (RenderWindow)sender;
            switch (e.Code) {
                case (Keyboard.Key.Escape): sWindow.Close(); break;
                case (Keyboard.Key.F1): Core.DebugInfo(); break;
                case (Keyboard.Key.Add): if (Core.isDebugInfo) tickDelay -= 1; break;
                case (Keyboard.Key.Subtract): if (Core.isDebugInfo) tickDelay += 10; break;
                case (Keyboard.Key.Multiply): if (Core.isDebugInfo) tickDelay = 16; break;
                case (Keyboard.Key.Divide): if (Core.isDebugInfo) isVsync = !isVsync; window.SetVerticalSyncEnabled(isVsync); break;
            }
        }

		static void FPS () {
			if ((int)clock.ElapsedMilliseconds != 0) HUDFPS.valueP = 1000 / (int)clock.ElapsedMilliseconds;
			else HUDFPS.valueP = 9000;
		}

		static void OnClose (object sender, EventArgs e) {
			RenderWindow sWindow = (RenderWindow)sender;
			sWindow.Close ();
		}

		public static int Distance (Vector2f vector1, Vector2f vector2) {
			return((int)Math.Sqrt (Math.Pow ((vector2.X - vector1.X), 2) + Math.Pow ((vector2.Y - vector1.Y), 2)));
		}

		public static void GameOver () {
            camera = new SFML.Graphics.View(new FloatRect(0, 0, windowWidth, windowHeight));
            Level.Destroy();
            cameraSpeed = 0;
            new GOMenu();
            Core.HUDFPS.Position = new Vector2f(0, 0);
            Core.HUDVsync.Position = new Vector2f(100, 0);
		}

        public static void NewGame() {
            camera = new SFML.Graphics.View(new FloatRect(0, 0, windowWidth, windowHeight));
            cameraSpeed = defaultCameraSpeed;
            Level.Create();
        }

        public static void Menu() {
            camera = new SFML.Graphics.View(new FloatRect(0, 0, windowWidth, windowHeight));
            cameraSpeed = 0;
            new MainMenu();
            Core.HUDFPS.Position = new Vector2f(0, 0);
            Core.HUDVsync.Position = new Vector2f(100, 0);

        }

		static void TickHandler() {
			camera.Move (new Vector2f (0, cameraSpeed));
			window.SetView (camera);
			if (tickCounterLimit == tickCounter) tickCounter = 1;
			else tickCounter++;
			if (isVsync) HUDVsync.valueP = 1;
			else HUDVsync.valueP = 0;
		}

        public static void DebugInfo() {
            if (isDebugInfo) isDebugInfo = false;
            else isDebugInfo = true;
        }

        public static bool Pause {
            set { isPause = value; }
            get { return isPause; }
        }

        public static void Initialize() {
            window.KeyPressed += new EventHandler<SFML.Window.KeyEventArgs>(OnKeyPressed);
            window.Closed += new EventHandler(OnClose);
            Tick += new VoidDelegate(TickHandler);
            window.SetVerticalSyncEnabled(isVsync);
            windowColor = Color.Black;
            try { Resources.Load(); }
            catch (SFML.LoadingFailedException) {
                MessageBox.Show("������� ����������.\n���������� ������ ����������", "������ ��������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                window.Close();
            }
            clock.Start();
            HUDFPS = new HUD(new Vector2f(0, 0), "FPS=", true);
            HUDVsync = new HUD(new Vector2f(100, 0), "Vsync=", true);
        }

        public static bool DebugInfoP { get { return (isDebugInfo); }}
	}
}
