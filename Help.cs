﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
    static class Help
    {
        static Sprite sprite;

        public static void Create() {
            sprite = new Sprite(Resources.controls);
            sprite.Position = new Vector2f(0, 200);
            Core.Draw += new Core.ByteDelegate(Draw);
        }

        public static void Dispose() {
            sprite.Dispose();
            Core.Draw -= new Core.ByteDelegate(Draw);
        }

        public static void Draw(byte layer) {
            if (layer == 4) Core.window.Draw(sprite);
        }
    }
}
