using System;
using SFML.Window;
using SFML.Graphics;
using SFML.Audio;

namespace YetAnotherScrollShooter
{
	public abstract class Bullet:Entity
	{
        protected Sound sound;
        protected bool isAlly,isActive=true;
        protected int speed,damage;

        protected override void TickHandler() {
            base.TickHandler();
            if (isAlly) position.Y -= speed;
            else position.Y += speed;
            if (!isActive) Dispose();
        }

        public int Damage { get { return (damage); } }

        public virtual void Destroy() {
			Vector2f tempPosition = position;
			tempPosition.Y -= height / 2;
			tempPosition.X -= width / 2;
			new SmallExplosion (tempPosition);
            isActive = false;
        }

        protected override void Dispose() {
            if (isAlly) Core.allyBulletsList.Remove(this);
            else Core.enemyBulletsList.Remove(this);
            if (sound!=null) sound.Dispose();
            base.Dispose();
        }

		public Vector2f Position { 
			get {
				Vector2f tempPosition = position;
				tempPosition.X += width / 2;
				tempPosition.Y += height / 2;
				return (tempPosition);
			}
		}
	}

    public class Plasmoid:Bullet
    {
        Sound sound = new Sound(Resources.plasmoidSound);

        public Plasmoid(Vector2f bulletPosition, bool ally) {
			targetLayer = 1;
            damage = 22;
            this.isAlly = ally;
            maxAnimationFrames = 1;
            width = 7;
            height = 15;
            this.position = bulletPosition;
			if (isAlly) texture = Resources.bPlasmoidTexture;
			else texture = Resources.gPlasmoidTexture;
            MakeSprite();
            speed = 12;
            if (isAlly) { Core.allyBulletsList.Add(this); position.X -= width / 2; }
            else { Core.enemyBulletsList.Add(this); sprite.Rotation = 180; position.X += width / 2; }
            Sound sound = new Sound(Resources.plasmoidSound);
            sound.Play();
        }
    }

    public class Missile : Bullet
    {
        Sound sound = new Sound(Resources.missileSound);

        public Missile(Vector2f missilePosition, bool ally) {
			targetLayer = 1;
            damage = 120;
            this.isAlly = ally;
            maxAnimationFrames = 3;
            width = 11;
            height = 29;
            missilePosition.Y -= height;
            missilePosition.X -= width / 2;
            this.position = missilePosition;
            texture = Resources.missileTexture;
            MakeSprite();
            speed = 10;
            if (isAlly) { Core.allyBulletsList.Add(this); }
            else { Core.enemyBulletsList.Add(this); sprite.Rotation = 180; position.X += width; position.Y += height; }
            sound.Play();
        }
    }
}

