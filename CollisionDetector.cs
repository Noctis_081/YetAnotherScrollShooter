using System;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
	public static class CollisionDetector
	{
		public static void TickHandler() {
			FloatRect enemyBounds, allyBounds, itemBounds;

			foreach (Vessel ally in Core.alliesList){
				allyBounds = ally.sprite.GetGlobalBounds ();
				foreach (Vessel enemy in Core.enemiesList) {
					enemyBounds = enemy.sprite.GetGlobalBounds ();
					if (allyBounds.Intersects (enemyBounds)) {
						enemy.Hit (9999);
						ally.Hit (120);
					}
				}
			}

            foreach (Vessel ally in Core.alliesList) {
                allyBounds = ally.sprite.GetGlobalBounds();
                foreach (Bullet enemy in Core.enemyBulletsList) {
                    enemyBounds = enemy.sprite.GetGlobalBounds();
                    if (allyBounds.Intersects(enemyBounds)) {
                        enemy.Destroy();
                        ally.Hit(enemy.Damage);
                    }
                }
            }

            foreach (Bullet ally in Core.allyBulletsList) {
                allyBounds = ally.sprite.GetGlobalBounds();
                foreach (Vessel enemy in Core.enemiesList) {
                    enemyBounds = enemy.sprite.GetGlobalBounds();
                    if (allyBounds.Intersects(enemyBounds)) {
                        enemy.Hit(ally.Damage);
                        ally.Destroy();
                    }
                }
            }

			foreach (Vessel ally in Core.alliesList) {
				allyBounds = ally.sprite.GetGlobalBounds();
				foreach (Item item in Core.itemsList) {
					itemBounds = item.sprite.GetGlobalBounds();
					if (allyBounds.Intersects(itemBounds)) {
						item.Use ();
						item.Destroy();
					}
				}
			}
		}
	}
}

