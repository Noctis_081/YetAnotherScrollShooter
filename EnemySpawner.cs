using System;
using System.IO;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
	public static class EnemySpawner
	{
        static StreamReader patternReader = Resources.enemySpawnPattern;
        static Random random = new Random();
        const int gridWidth = 5;
        const int spawnStep = Core.windowWidth / gridWidth;
        const int spawnOffset = 70;
        const int spawnDelayMultiplier = 1;
        static bool isSpawnAllowed = false;
        static string[] spawnTypes = new string[5];
        static int nextSpawnTick;
        static int tickCounter;
        static int patternsCount=PatternsCount();
        static char[] validLetters = { 'C', 'S', 'T', 'F', 'I', 'B', 'M', 'N', 'U' };
        

        public static void SpawnLine() {
            String tempString = patternReader.ReadLine();
            if (!tempString.StartsWith("ENDOFPATTERN")) {
                ReadPatternLine(tempString);
                int currentPosX = spawnOffset;
                for (int i = 0; i < gridWidth; i++) {
                    switch (spawnTypes[i]) {
                        case "SC": new Scout(new Vector2f(currentPosX, Core.camera.Center.Y - Core.windowHeight / 2)); break;
                        case "ST": new Striker(new Vector2f(currentPosX, Core.camera.Center.Y - Core.windowHeight / 2)); break;
                        case "FI": new Fighter(new Vector2f(currentPosX, Core.camera.Center.Y - Core.windowHeight / 2)); break;
                        case "BM": new Bomber(new Vector2f(currentPosX, Core.camera.Center.Y - Core.windowHeight / 2)); break;
                        case "IN": new Interceptor(new Vector2f(currentPosX, Core.camera.Center.Y - Core.windowHeight / 2)); break;
                    }
                    currentPosX += spawnStep;
                }
            }
        }

        public static void SpawnFromPattern(int pattern) {
            SearchForPattern(pattern);
            tickCounter = 0;
            SpawnLine();
        }

        public static void TickHandler() {
            if (isSpawnAllowed) {
                if (Core.enemiesList.Count == 0) {
                    int temp = random.Next(1, patternsCount + 1);
                    SpawnFromPattern(temp);
                }
                tickCounter++;
                if (tickCounter == nextSpawnTick * spawnDelayMultiplier) SpawnLine();
            }
        }

        public static void ReadPatternLine(string input) {
            nextSpawnTick = int.Parse(input.Substring(0, input.IndexOf(' ')));
            input = input.Substring(input.IndexOfAny(validLetters));
            for (int i = 0; i < gridWidth; i++) {
                spawnTypes[i] = input.Substring(0, 2);
                input = input.Substring(3);
            }
        }

        public static bool SearchForPattern(int query) {
            String tempString;
            String outputString;
            int pointer;
            int temp=-1;
            patternReader.DiscardBufferedData();
            patternReader.BaseStream.Position = 0;
            while (!patternReader.EndOfStream) {
                tempString = patternReader.ReadLine();
                pointer = 0;
                outputString = "";
                if (tempString.StartsWith("PATTERN")) {
                    pointer = tempString.IndexOf(' ');
                    while (tempString[++pointer] != ';') outputString += tempString[pointer];
                }
                int.TryParse(outputString, out temp);
                if (query == temp) break;
            }
            return temp == query;
        }

        public static int PatternsCount() {
            int output = 0;
            patternReader.DiscardBufferedData();
            patternReader.BaseStream.Position=0;
            while (!patternReader.EndOfStream) if (patternReader.ReadLine().StartsWith("ENDOFPATTERN")) output++;
            return output;
        }

        public static bool AllowSpawn {
            set {
                isSpawnAllowed = value;
                if (isSpawnAllowed) nextSpawnTick=-1;
            }
            get { return isSpawnAllowed; }
        }
	}
}

