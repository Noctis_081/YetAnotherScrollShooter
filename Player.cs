using System;
using SFML.Window;
using SFML.Graphics;

namespace YetAnotherScrollShooter
{
	public class Player:Vessel
	{
		bool[] keyState=new bool[5];
        HUD HUDHP, HUDPosX, HUDPosY, HUDMissile, HUDGodMode, HUDShields, HUDMissileRus;
        GraphicalHUD GHUDHP, GHUDSP;

		public Player () {
            GHUDHP = new GraphicalHUD(new Vector2f(Core.camera.Center.X - Core.windowWidth / 2, Core.camera.Center.Y + Core.windowHeight / 2 - 32), Color.Green);
            GHUDSP = new GraphicalHUD(new Vector2f(Core.camera.Center.X - Core.windowWidth / 2 + 150, Core.camera.Center.Y + Core.windowHeight / 2 - 32), Color.Blue);
            HUDMissileRus = new HUD(new Vector2f(Core.camera.Center.X - Core.windowWidth / 2, Core.camera.Center.Y + Core.windowHeight / 2 - 64),"������: ",false);
            HUDMissileRus.Font = Resources.menuFont;
            HUDMissileRus.Size = 30;
            position = Core.camera.Center;
            position.X -= width / 2;
            maxHP = 100;
            hp = maxHP;
			maxSP = 100;
			sp = maxSP;
            missilesAmount = 5;
            isAlly = true;
			Core.alliesList.Add (this);
			texture=Resources.playerTexture;
			width = 43;
			height = 64;
			MakeSprite ();
			speed = 6;
			weaponDelay = 10;
            Core.window.KeyPressed += new EventHandler<KeyEventArgs>(this.OnKeyPressed);
            Core.window.KeyReleased += new EventHandler<KeyEventArgs>(this.OnKeyReleased);
            HUDPosX = new HUD(new Vector2f(0, 30), "Player POS.X=", true);
            HUDPosY = new HUD(new Vector2f(0, 45), "Player POS.Y=", true);
            HUDHP = new HUD(new Vector2f(0, 60), "Player HP=", true);
            HUDMissile = new HUD(new Vector2f(0, 75), "Player Missiles=", true);
            HUDGodMode = new HUD(new Vector2f(0, 90), "Player GodMode=", true);
			HUDShields = new HUD (new Vector2f (120, 60), "SP=", true);
		}

		void OnKeyPressed(object sender,KeyEventArgs e) {
			switch (e.Code) {
				case (Keyboard.Key.W): keyState [0] = true; break;
				case (Keyboard.Key.S): keyState [1] = true; break;
				case (Keyboard.Key.A): keyState [2] = true; break;
				case (Keyboard.Key.D): keyState [3] = true; break;
                case (Keyboard.Key.Up): keyState[0] = true; break;
                case (Keyboard.Key.Down): keyState[1] = true; break;
                case (Keyboard.Key.Left): keyState[2] = true; break;
                case (Keyboard.Key.Right): keyState[3] = true; break;
				case (Keyboard.Key.Space): keyState[4]=true; break;
				case (Keyboard.Key.H): if (Core.isDebugInfo) Heal (10); break;
                case (Keyboard.Key.N): if (Core.isDebugInfo) AddMissile(5); break;
                case (Keyboard.Key.LAlt): shootMissile(); break;
                case (Keyboard.Key.G): if (Core.isDebugInfo) GodMode(); break;
			}
		}

		void OnKeyReleased(object sender,KeyEventArgs e) {
			switch (e.Code) {
				case (Keyboard.Key.W): keyState [0] = false; break;
				case (Keyboard.Key.S): keyState [1] = false; break;
				case (Keyboard.Key.A): keyState [2] = false; break;
				case (Keyboard.Key.D): keyState [3] = false; break;
                case (Keyboard.Key.Up): keyState[0] = false; break;
                case (Keyboard.Key.Down): keyState[1] = false; break;
                case (Keyboard.Key.Left): keyState[2] = false; break;
                case (Keyboard.Key.Right): keyState[3] = false; break;
				case (Keyboard.Key.Space): keyState[4]=false; break;
			}
		}

		override protected void TickHandler() {
            GHUDHP.Percentage = hp;
            GHUDSP.Percentage = sp;
			HUDHP.valueP = hp;
			HUDShields.valueP = sp;
			HUDPosX.valueP = (int)position.X+width/2;
			HUDPosY.valueP = (int)position.Y+height/2;
            HUDMissile.valueP = missilesAmount;
            HUDMissileRus.valueP = missilesAmount;
			if (isGodMode) HUDGodMode.valueP = 1;
			else HUDGodMode.valueP = 0;
			if (keyState [0]&&(Core.camera.Center.Y-position.Y<(Core.windowHeight/2-speed))) Move('u');
			if (keyState [1]&&(Core.camera.Center.Y-position.Y>-(Core.windowHeight/2-height-speed))) Move('d');
			if (keyState [2]&&(Core.camera.Center.X-position.X<(Core.windowWidth/2-speed))) Move('l');
			if (keyState [3]&&(Core.camera.Center.X-position.X>-(Core.windowWidth/2-width-speed))) Move('r');
			if (keyState [4]) Shoot ();
            base.TickHandler();
		}

		public Vector2f Position { get {return this.position;} }

		public int Width { get {return this.width;}}

		public int Height { get {return this.height;}}

        public override void Destroy() {
            HUDHP.Destroy();
            HUDPosX.Destroy();
            HUDPosY.Destroy();
            HUDMissile.Destroy();
            HUDShields.Destroy();
            HUDGodMode.Destroy();
            HUDMissileRus.Destroy();
            GHUDHP.Destroy();
            GHUDSP.Destroy();
            base.Destroy();
            Core.alliesList.Remove(this);
            EnemySpawner.AllowSpawn = false;
            Core.GameOver();
        }

		override protected void Dispose() {
			base.Dispose();
			Core.window.KeyPressed -= new EventHandler<KeyEventArgs> (this.OnKeyPressed);
			Core.window.KeyReleased -= new EventHandler<KeyEventArgs> (this.OnKeyReleased);
		}

		void GodMode() {
			if (isGodMode) isGodMode = false;
			else isGodMode=true;
		}
	}
}

