using System;
using SFML.Graphics;
using SFML.Window;
using SFML.Audio;

namespace YetAnotherScrollShooter
{
	public abstract class Effect:Entity
	{
        protected Sound sound;
		public Effect() {
			targetLayer = 3;
			height = 64;
			width = height;
			animationDelay = 10;
			maxAnimationFrames = 3;
		}

		protected override void NextAnimationFrame () {
			animationFrame++;
			textureWindow.Left += width;
			sprite.TextureRect = textureWindow;
			if (animationFrame > maxAnimationFrames) Dispose ();
		}

        protected override void Dispose() {
            if (sound != null) sound.Dispose();
            base.Dispose();
        }
	}

	public class Explosion:Effect
	{
        Sound sound = new Sound(Resources.bigExplosionSound);

		public Explosion(Vector2f position) {
			texture = Resources.bigExplosionTexture;
			maxAnimationFrames = 5;
			this.position = position;
			this.position.Y -= height / 2;
			this.position.X -= width / 2;
			MakeSprite ();
            sound.Play();
		}
	}

	public class SmallExplosion:Effect
	{
        Sound sound = new Sound(Resources.smallExplosionSound);

		public SmallExplosion(Vector2f position) {
			texture = Resources.smallExplosionTexture;
			maxAnimationFrames = 2;
			this.position = position;
			this.position.Y -= height / 2;
			this.position.X -= width / 2;
			MakeSprite ();
            sound.Play();
		}
	}

	public class HealEffect:Effect
	{
        Sound sound = new Sound(Resources.plasmoidSound);

		public HealEffect(Vector2f position) {
			texture = Resources.healthEffectTexture;
			this.position = position;
			this.position.Y -= height / 2;
			this.position.X -= width / 2;
			MakeSprite ();
		}
	}

	public class ShieldEffect:Effect
	{
        Sound sound = new Sound(Resources.plasmoidSound);

		public ShieldEffect(Vector2f position) {
			texture = Resources.shieldEffectTexture;
			this.position = position;
			this.position.Y -= height / 2;
			this.position.X -= width / 2;
			MakeSprite ();
		}
	}

	public class AmmoEffect:Effect
	{
        Sound sound = new Sound(Resources.plasmoidSound);

		public AmmoEffect(Vector2f position) {
			texture = Resources.ammoEffectTexture;
			this.position = position;
			this.position.Y -= height / 2;
			this.position.X -= width / 2;
			MakeSprite ();
		}
	}
}

