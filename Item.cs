using System;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
	public abstract class Item:Entity
	{
		protected int resourceAmount;
		bool isActive = true;

		public Item (Vector2f position) {
			width = 32;
			height = 32;
			this.position = position;
			this.position.X -= width / 2;
			this.position.Y -= height/2;
			maxAnimationFrames = 1;
			Core.itemsList.Add (this);
		}

		public void Destroy() {
			isActive = false;
		}

		protected override void Dispose() {
			Core.itemsList.Remove (this);
			base.Dispose();
		}

		protected override void TickHandler () {
			base.TickHandler ();
			if (!isActive||!EnemySpawner.AllowSpawn) Dispose ();
		}

		public abstract void Use ();
	}

	public class Health:Item 
	{
		public Health(Vector2f position):base(position) {
			resourceAmount = 45;
			texture = Resources.healthItemTexture;
			MakeSprite ();
		}

		public override void Use () {
			Vector2f tempPosition = position;
			tempPosition.X += width / 2;
			tempPosition.Y += height / 2;
			new HealEffect (tempPosition);
			Level.player.Heal (resourceAmount);
		}
	}

	public class Ammo:Item 
	{
		public Ammo(Vector2f position):base(position) {
			resourceAmount = 5;
			texture = Resources.ammoItemTexture;
			MakeSprite ();
		}

		public override void Use () {
			Vector2f tempPosition = position;
			tempPosition.X += width / 2;
			tempPosition.Y += height / 2;
			new AmmoEffect (tempPosition);
            Level.player.AddMissile(resourceAmount);
		}
	}

	public class Shield:Item 
	{
		public Shield(Vector2f position):base(position) {
			resourceAmount = 45;
			texture = Resources.shieldItemTexture;
			MakeSprite ();
		}

		public override void Use () {
			Vector2f tempPosition = position;
			tempPosition.X += width / 2;
			tempPosition.Y += height / 2;
			new ShieldEffect (tempPosition);
            Level.player.RestoreShields(100);
		}
	}
}