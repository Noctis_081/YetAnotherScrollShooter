using System;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
	public class HUD
	{
		Vector2f position;
		Text text;
		string valueName;
        int valueValue;
        bool toDispose = false, isVisible=true, showValue=false, isDebug=true;

		public HUD(Vector2f pos, string name, bool isDebug) {
			this.isDebug = isDebug;
			valueValue = 0;
			valueName = name;
			position = pos;
			text = new Text(name,Resources.hudFont);
			text.CharacterSize = 12;
			text.Color = Color.White;
			Core.Draw += new Core.ByteDelegate (this.Draw);
			Core.Tick += new Core.VoidDelegate (this.TickHandler);
		}

		void TickHandler() {
			if (showValue) text.DisplayedString = valueName + valueValue.ToString ();
			else text.DisplayedString = valueName;
			position.Y += Core.cameraSpeed;
            if ((Core.Distance(Core.camera.Center, this.position) > 1000)) this.Dispose();
            if (toDispose) Dispose();
		}

		public int valueP {
			set {
				showValue = true; 
				valueValue = value;
			}
		}

        public int Size { set { text.CharacterSize = (uint)value; } }

        public Vector2f Position { set { position = value; } }

        public Font Font { set { text.Font = value; } }

		public void Hide() {
			isVisible = false;
		}

		public void Show() {
			isVisible = true;
		}

        public void Destroy() {
            toDispose = true;
        }

		public void Dispose() {
			Core.Draw -= new Core.ByteDelegate (this.Draw);
			Core.Tick -= new Core.VoidDelegate (this.TickHandler);
			text.Dispose ();
		}

		void Draw(byte layer) {
			if (isVisible&(Core.DebugInfoP|(!isDebug))) {
				if (layer == 4) {
					text.Position = position;
					Core.window.Draw (text);
				}
			}
		}

	}
}

