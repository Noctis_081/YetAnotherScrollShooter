﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;

namespace YetAnotherScrollShooter
{
    public class MainMenu
    {
        protected Sprite bgSprite;
        protected HUD newGameButton, exitButton, helpButton, pointer, score;
        protected byte opacity, selection;
        protected bool isShowingHelp, showScore;

        public MainMenu() {
            isShowingHelp = false;
            showScore = false;
            selection = 1;
            bgSprite = new Sprite(Resources.mainMenuSplash);
            opacity = 0;
            bgSprite.Color = new Color(255, 255, 255, opacity);
            pointer = new HUD(new Vector2f(170, 200), ">", false);
            pointer.Font = Resources.menuFont;
            pointer.Size = 40;
            pointer.Hide();
            newGameButton = new HUD(new Vector2f(200, 200), "Начать игру", false);
            newGameButton.Font = Resources.menuFont;
            newGameButton.Hide();
            newGameButton.Size = 40;
            helpButton = new HUD(new Vector2f(200, 300), "Помощь", false
                );
            helpButton.Font = Resources.menuFont;
            helpButton.Hide();
            helpButton.Size = 40;
            exitButton = new HUD(new Vector2f(200, 400), "Выход", false);
            exitButton.Font = Resources.menuFont;
            exitButton.Hide();
            exitButton.Size = 40;
            score = new HUD(new Vector2f(200, 140), "Счет: ", false);
            score.Font = Resources.menuFont;
            score.Hide();
            score.Size = 30;
            score.valueP = Level.score;
            Core.Tick += new Core.VoidDelegate(TickHandler);
            Core.Draw += new Core.ByteDelegate(Draw);
            Core.window.KeyPressed += new EventHandler<KeyEventArgs>(OnKeyPressed);
        }

        public void Destroy() {
            bgSprite.Dispose();
            newGameButton.Destroy();
            exitButton.Destroy();
            pointer.Destroy();
            helpButton.Destroy();
            score.Destroy();
            Core.Tick -= new Core.VoidDelegate(TickHandler);
            Core.Draw -= new Core.ByteDelegate(Draw);
            Core.window.KeyPressed -= new EventHandler<KeyEventArgs>(OnKeyPressed);
        }

        protected void TickHandler() {
            if (opacity < 255) {
                bgSprite.Color = new Color(255, 255, 255, opacity);
                opacity += 3;
            }
            else {
                if (showScore) score.Show();
                exitButton.Show();
                newGameButton.Show();
                pointer.Show();
                helpButton.Show();
            }
            switch (selection) {
                case 1: pointer.Position = new Vector2f(170, 200); break;
                case 2: pointer.Position = new Vector2f(170, 300); break;
                case 3: pointer.Position = new Vector2f(170, 400); break;
            }
        }

        protected void OnKeyPressed(object sender, KeyEventArgs e) {
            RenderWindow sWindow = (RenderWindow)sender;
            opacity = 252;
            switch (e.Code) {
                case (Keyboard.Key.W): SelectonUp(); break;
                case (Keyboard.Key.S): SelectionDown(); break;
                case (Keyboard.Key.Up): SelectonUp(); break;
                case (Keyboard.Key.Down): SelectionDown(); break;
                case (Keyboard.Key.Return): Execute();  break;
                case (Keyboard.Key.Space): Execute(); break;
            }
        }

        protected void Draw(byte layer) {
            if (layer == 0) { Core.window.Draw(bgSprite); }
        }

        protected void Execute() {
            switch (selection) {
                case 1: Destroy(); Core.NewGame(); break;
                case 2: if (isShowingHelp) Help.Dispose(); else Help.Create(); isShowingHelp = !isShowingHelp; break;
                case 3: Core.window.Close(); break;
            }
        }

        protected void SelectonUp() {
            if (!isShowingHelp) {
                if (selection == 1) selection = 3;
                else selection--;
            }
        }

        protected void SelectionDown() {
            if (!isShowingHelp) {
                if (selection == 3) selection = 1;
                else selection++;
            }
        }
    }

    public class GOMenu : MainMenu
    {
        public GOMenu() {
            showScore = true;
            newGameButton = new HUD(new Vector2f(200, 200), "Новая игра", false);
            newGameButton.Font = Resources.menuFont;
            newGameButton.Hide();
            newGameButton.Size = 40;
            bgSprite = new Sprite(Resources.gameOverSplash);
            opacity = 0;
            bgSprite.Color = new Color(255, 255, 255, opacity);
        }
    }
}
